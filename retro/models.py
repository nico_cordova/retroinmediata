from django.db import models
from retro_auth.models import UserProfile
from django.utils import timezone

# Create your models here.

class Subject(models.Model):
    name = models.CharField(max_length=100, default="No Name")
    api_pk = models.CharField(max_length=4,default=0)

class Section(models.Model):
    api_pk = models.CharField(max_length=4,default=0)
    teacher = models.ForeignKey(UserProfile,blank=True,null=True,on_delete=models.SET_NULL)
    students = models.ManyToManyField(UserProfile,related_name="students",blank=True)
    subject = models.ForeignKey(Subject,blank=True,null=True,on_delete=models.SET_NULL)

class Post(models.Model):
    title = models.CharField(max_length=200,default="")
    description = models.CharField(max_length=500,default="")
    author = models.ForeignKey(UserProfile,blank=True,null=True,on_delete=models.SET_NULL)
    publish_date = models.DateTimeField(default=timezone.now)
    last_mod = models.DateTimeField(default=timezone.now)
    
class Comment(models.Model):
    parent = models.ForeignKey("Comment",blank=True,default=None,on_delete=models.CASCADE)
    description = models.CharField(max_length=500,default="")
    author = models.ForeignKey(UserProfile,blank=True,null=True,on_delete=models.SET_NULL)
    publish_date = models.DateTimeField(default=timezone.now)
    last_mod = models.DateTimeField(default=timezone.now)
