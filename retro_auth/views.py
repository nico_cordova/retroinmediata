from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.urls import reverse
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.contrib import messages
from retro_auth.models import UserProfile
import datetime
import ast
import os
import requests

def auth_login(request):
    logout(request)
    username = password = ''
    if request.POST:
        request.session["h"] = 0
        username = request.POST["username"]
        password = request.POST["password"]
        post_data = {
            'username': username,
            'password': request.POST["password"]
        }
        response = requests.post(
            os.getenv("AUTH_URL")+"/rest-auth/login/", data=post_data)
        content = response.content
        content = ast.literal_eval(content)
        if "key" in content:
            post_data = {'username': username}
            header = {"Content-Type": 'application/json',
                        "Authorization": "Token " + content["key"]}
            response_1 = requests.get(
                os.getenv("AUTH_URL")+'/rest-auth/user/', headers=header, data=post_data)
            content_1 = ast.literal_eval(response_1.content)
            request.session["name"] = content_1["first_name"] + \
                " " + content_1["last_name"]
            request.session["token"] = content["key"]
            header = {"Content-Type": 'application/x-www-form-urlencoded',
                        'Authorization': "Token " + content["key"]}
            post_data = {'user_pk': content_1['pk'],
                            'period': datetime.date.today().year}
            response_2 = requests.post(
                os.getenv("AUTH_URL")+'/get/get_user_info/', headers=header, data=post_data)
            content_2 = ast.literal_eval(response_2.content)
            content_2 = content_2['user_info']
            request.session["user_pk"] = content_1['pk']
            request.session["username"] = username
            request.session["rut"] = content_2['user_rut']
            request.session["type"] = content_2['user_type']
            if content_1['first_name'][0] == " ":
                first = content_1['first_name'][1:].split(" ")
            else:
                first = content_1['first_name'].split(" ")

            if content_1["last_name"][0] == " ":
                last = content_1["last_name"][1:].split(" ")
            else:
                last = content_1["last_name"].split(" ")

            full_name = ""
            for name in first:
                full_name += name.capitalize()+" "

            for name in last:
                full_name += name.capitalize()+" "

            if not User.objects.filter(username=username).exists():
                n_usuario = User.objects.create_user(
                    username=username,
                    password=password
                )
                n_usuario.save()
                n_usuario.first_name = content_1['first_name']
                n_usuario.last_name = content_1['last_name']
                n_usuario.save()

                if request.session["type"] == "AL":
                    try:
                        profile = UserProfile.objects.get(
                            rut = content_2['user_rut']
                        )
                        profile.api_pk = content_1["pk"]
                        profile.user = n_usuario
                        profile.save()

                    except UserProfile.DoesNotExist:
                        profile = UserProfile.objects.create(
                            api_pk = content_1['pk'],
                            user=n_usuario,
                            name=full_name,
                            user_type="AL",
                            carreer=content_2["carreer_pk"],
                            rut=content_2['user_rut'],
                        )
                        profile.save()
                else:
                    try:
                        profile = UserProfile.objects.get(
                            rut = content_2['user_rut']
                        )
                        profile.api_pk = content_1["pk"]
                        profile.user = n_usuario
                        profile.save()

                    except UserProfile.DoesNotExist:
                        profile = UserProfile.objects.create(
                            api_pk = content_1['pk'],
                            user=n_usuario,
                            name=full_name,
                            user_type="DO",
                            carreer=content_2["carreer_pk"],
                            rut=content_2['user_rut'],
                        )
                        profile.save()
                        
                login(request, n_usuario)

                return HttpResponseRedirect(reverse_lazy('index'))
            else:
                user = authenticate(
                    username=username,
                    password=password
                )
                login(request, user)
                return HttpResponseRedirect(reverse_lazy('index'))
        else:
            pass
            #APLICAR DJANGO MESSAGE FRAMEWORK
    data = {}
    template_name = "login.html"
    return render(request, template_name, data)


def auth_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))
